FROM debian:9 as build
RUN apt update && apt install -y wget gcc g++ make libpcre3 libpcre3-dev zlib1g zlib1g-dev libssl-dev
RUN wget https://nginx.org/download/nginx-1.21.3.tar.gz && tar xvfz nginx-1.21.3.tar.gz \
    && cd ./nginx-1.21.3/ && ./configure  && make && make install

FROM debian:9
WORKDIR /usr/local/nginx/sbin
COPY --from=build /usr/local/nginx/sbin/nginx .
COPY --from=build /usr/local/nginx/conf /usr/local/nginx/conf
RUN mkdir ../logs && touch ../logs/error.log && chmod +x nginx
ADD ./nginx.conf /usr/local/nginx/nginx.conf
ADD ./index.html /var/www/
ADD ./nginx.conf /usr/local/nginx/conf/nginx.conf
CMD ["./nginx", "-g", "daemon off;"]





